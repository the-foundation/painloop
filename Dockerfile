#rainloop-docker dockerfile
#FROM php:7.4-apache
#FROM jtreminio/php:7.4
#FROM php:7.4.0-fpm-alpine
FROM quay.io/thefoundation/hocker:php8.1-dropbear-fpm

MAINTAINER The Foundation <gitlab.io@commit.aleeas.com>

#RUN apt-get update && apt -y install lsb-release apt-transport-https ca-certificates
# && curl  https://packages.sury.org/php/apt.gpg > /etc/apt/trusted.gpg.d/php.gpg && sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'  &&  apt-get update &&
# && add-apt-repository ppa:ondrej/php &&

#php7.4-json not possible with 8.1
# Download rainloop source
#RUN curl -O http://www.rainloop.net/repository/webmail/rainloop-community-latest.zip
# Install unzip and extract the rainloop files to the actual webserver folder
COPY get-painloop.sh /get-painloop.sh
RUN /bin/bash /get-painloop.sh
RUN apt-get update && apt-get install --no-install-recommends -y \
    unzip php8.1 php8.1-mysql php8.1-curl  php8.1-cgi libapache2-mod-php8.1 php8.1-xmlrpc php8.1-gd php8.1-mbstring php8.1 php8.1-common php8.1-xmlrpc php8.1-soap php8.1-xml php8.1-intl php8.1-cli php8.1-ldap php8.1-zip php8.1-readline php8.1-imap php8.1-tidy php8.1-intl php8.1-sqlite3  php8.1-fpm \
    && cd /var/www/html \
    && find . -type d -exec chmod 755 {} \; \
    && find . -type f -exec chmod 644 {} \; \
    && /bin/bash -c 'find /var/lib/apt/lists/ -type f -delete || true' &&  chown -R www-data:www-data . && ( apt-get clean all ; rm -rf /var/lib/dpkg/lists /var/cache/apt/archives ||true )

RUN sed 's/options: \[2048, 4096\]/options: [4096, 8192]/g' /var/www/html/rainloop/v/*/app/templates/Views/User/PopupsNewOpenPgpKey.html -i||true
RUN sed 's/minRSABits:2047/minRSABits:4095/g;s/minRSABits: 2047/minRSABits: 4095/g' /var/www/html/snappymail/v/*/static/js/openpgp.js /var/www/html/snappymail/v/*/static/js/min/openpgp.min.js  -i||true


RUN /bin/bash -c 'chmod -R a+w $(find /var/www/html/snappymail/v/ -type f |grep css$)' &&  /bin/bash -c "cd /var/www/html;grep -rl '0,0,0,0..'|grep themes/|while read a ;do sed 's/0,0,0,0../0,0,0,0.8/g;s/0, 0, 0, 0../0, 0, 0, 0.8/g;s/opacity: 0../opacity: 0.8/g' -i "'$a'";done;sed 's/0,0,0,0.7/0,0,0,0.8/g;s/0, 0, 0, 0.7/0, 0, 0, 0.8/g;s/opacity: 0.7/opacity: 0.8/g' -i /var/www/html/snappymail/v/*/themes/*/styles.less /var/www/html/snappymail/v/*/static/css/app.css $(find /var/www/html/snappymail/v/ -type f |grep css$) "  || true
RUN sed 's/@login-rgba-background-color:.\+/@login-rgba-background-color: rgba(255,255,255,0.8);/g;s/0,0,0,0../0,0,0,0.8/g;s/0, 0, 0, 0../0, 0, 0, 0.8/g' /var/www/html/snappymail/v/*/themes/*/styles.less -i || true 
RUN sed 's/,0.5/,0.8/g' -i /var/www/html/snappymail/v/*/static/*/* /var/www/html/snappymail/v/*/static/*/*/* /var/www/html/snappymail/v/*/app/templates/Themes/* || true
RUN chmod go-w $(find /var/www/html/snappymail/v/ -type f |grep css$)
## SVED POOLNIAR LL3H N1 NRUB


#RUN systemctl enable apache2
RUN mkdir -p /var/run/apache2
##SQUAT 1338
RUN sed 's/80/1338/g' /etc/apache2/sites-available/* /etc/apache2/ports.conf   -i
RUN echo "PFZpcnR1YWxIb3N0IDAuMC4wLjA6MTMzNz4KPElmTW9kdWxlIG1vZF9yZW1vdGVpcC5jPgpSZW1vdGVJUEludGVybmFsUHJveHkgMTcyLjE2LjAuMC8xMgpSZW1vdGVJUEludGVybmFsUHJveHkgMTY5LjI1NC4wLjAvMTYKUmVtb3RlSVBJbnRlcm5hbFByb3h5IDE5Mi4xNjguMC4wLzE2ClJlbW90ZUlQSW50ZXJuYWxQcm94eSAxMjcuMC4wLjAvOApSZW1vdGVJUEludGVybmFsUHJveHkgMTAuMC4wLjAvOApSZW1vdGVJUEludGVybmFsUHJveHkgZmU4MDo6LzEwClJlbW90ZUlQSW50ZXJuYWxQcm94eSBmZDAwOjovOApSZW1vdGVJUEludGVybmFsUHJveHkgZmMwMDo6LzcKUmVtb3RlSVBIZWFkZXIgWC1Gb3J3YXJkZWQtRm9yCjwvSWZNb2R1bGU+CgpIZWFkZXIgYWx3YXlzIHNldGlmZW1wdHkgUGVybWlzc2lvbnMtUG9saWN5ICJhY2NlbGVyb21ldGVyPSgpLCBhdXRvcGxheT0oc2VsZiksIGJhdHRlcnk9KHNlbGYpLCBjYW1lcmE9KCksIGNyb3NzLW9yaWdpbi1pc29sYXRlZD0oc2VsZiksIGRpc3BsYXktY2FwdHVyZT0oKSwgZG9jdW1lbnQtZG9tYWluPShzZWxmKSwgZW5jcnlwdGVkLW1lZGlhPShzZWxmKSwgZXhlY3V0aW9uLXdoaWxlLW5vdC1yZW5kZXJlZD0oc2VsZiksIGV4ZWN1dGlvbi13aGlsZS1vdXQtb2Ytdmlld3BvcnQ9KHNlbGYpLCBmdWxsc2NyZWVuPSosIGdlb2xvY2F0aW9uPShzZWxmKSwgZ3lyb3Njb3BlPSgpLCBtYWduZXRvbWV0ZXI9KCksIG1pY3JvcGhvbmU9KCksIG1pZGk9KCksIG5hdmlnYXRpb24tb3ZlcnJpZGU9KHNlbGYpLCBwYXltZW50PSgpLCBwaWN0dXJlLWluLXBpY3R1cmU9KiwgcHVibGlja2V5LWNyZWRlbnRpYWxzLWdldD0oc2VsZiksIHNjcmVlbi13YWtlLWxvY2s9KHNlbGYpLCBzeW5jLXhocj0oc2VsZiksIHVzYj0oKSwgd2ViLXNoYXJlPShzZWxmKSwgeHItc3BhdGlhbC10cmFja2luZz0oc2VsZiksIGNsaXBib2FyZC1yZWFkPSgpLCBjbGlwYm9hcmQtd3JpdGU9KHNlbGYpLCBnYW1lcGFkPShzZWxmKSwgc3BlYWtlci1zZWxlY3Rpb249KHNlbGYpLCBjb252ZXJzaW9uLW1lYXN1cmVtZW50PSgpLCBmb2N1cy13aXRob3V0LXVzZXItYWN0aXZhdGlvbj0oc2VsZiksIGhpZD0oc2xmKSwgaWRsZS1kZXRlY3Rpb249KHNlbGYpLCBzZXJpYWw9KCksIHN5bmMtc2NyaXB0PShzZWxmKSwgdHJ1c3QtdG9rZW4tcmVkZW1wdGlvbj0oc2VsZiksIHZlcnRpY2FsLXNjcm9sbD0oc2VsZikiCgpIZWFkZXIgYWx3YXlzIHNldGlmZW1wdHkgWC1GcmFtZS1PcHRpb25zICJTQU1FT1JJR0lOIgoKSGVhZGVyIGFsd2F5cyBzZXRpZmVtcHR5IFgtQ29udGVudC1UeXBlLU9wdGlvbnMgIm5vc25pZmYiCgpIZWFkZXIgYWx3YXlzIHNldGlmZW1wdHkgUmVmZXJyZXItUG9saWN5ICJuby1yZWZlcnJlci13aGVuLWRvd25ncmFkZSIKCkhlYWRlciBhbHdheXMgc2V0aWZlbXB0eSBYLVdlYktpdC1DU1AgICAgICAgICAgICAgICJkZWZhdWx0LXNyYyAnc2VsZicgaHR0cHM6IDsgc2NyaXB0LXNyYyAqICd1bnNhZmUtaW5saW5lJyAndW5zYWZlLWV2YWwnOyBzY3JpcHQtc3JjLWVsZW0gKiAqICd1bnNhZmUtaW5saW5lJyAndW5zYWZlLWV2YWwnIDsgc2NyaXB0LXNyYy1hdHRyICogKiAndW5zYWZlLWlubGluZScgJ3Vuc2FmZS1ldmFsJyA7IHN0eWxlLXNyYyAqIGRhdGE6ICd1bnNhZmUtaW5saW5lJzsgaW1nLXNyYyAqIGRhdGE6IDsgZm9udC1zcmMgKiBkYXRhOiA7IGNvbm5lY3Qtc3JjICo7IG1lZGlhLXNyYyAqOyBvYmplY3Qtc3JjICo7IHByZWZldGNoLXNyYyAqOyBjaGlsZC1zcmMgKjsgZnJhbWUtc3JjICo7IHdvcmtlci1zcmMgKjsgZnJhbWUtYW5jZXN0b3JzICo7IGZvcm0tYWN0aW9uICdzZWxmJzsgdXBncmFkZS1pbnNlY3VyZS1yZXF1ZXN0czsgYmFzZS11cmkgKjsgbWFuaWZlc3Qtc3JjICoiCgpIZWFkZXIgYWx3YXlzIHNldGlmZW1wdHkgQ29udGVudC1TZWN1cml0eS1Qb2xpY3kgICAiZGVmYXVsdC1zcmMgJ3NlbGYnIGh0dHBzOiA7IHNjcmlwdC1zcmMgKiAndW5zYWZlLWlubGluZScgJ3Vuc2FmZS1ldmFsJzsgc2NyaXB0LXNyYy1lbGVtICogKiAndW5zYWZlLWlubGluZScgJ3Vuc2FmZS1ldmFsJyA7IHNjcmlwdC1zcmMtYXR0ciAqICogJ3Vuc2FmZS1pbmxpbmUnICd1bnNhZmUtZXZhbCcgOyBzdHlsZS1zcmMgKiBkYXRhOiAndW5zYWZlLWlubGluZSc7IGltZy1zcmMgKiBkYXRhOiA7IGZvbnQtc3JjICogZGF0YTogOyBjb25uZWN0LXNyYyAqOyBtZWRpYS1zcmMgKjsgb2JqZWN0LXNyYyAqOyBwcmVmZXRjaC1zcmMgKjsgY2hpbGQtc3JjICo7IGZyYW1lLXNyYyAqOyB3b3JrZXItc3JjICo7IGZyYW1lLWFuY2VzdG9ycyAqOyBmb3JtLWFjdGlvbiAnc2VsZic7IHVwZ3JhZGUtaW5zZWN1cmUtcmVxdWVzdHM7IGJhc2UtdXJpICo7IG1hbmlmZXN0LXNyYyAqIgoKSGVhZGVyIGFsd2F5cyBzZXRpZmVtcHR5IFgtQ29udGVudC1TZWN1cml0eS1Qb2xpY3kgImRlZmF1bHQtc3JjICdzZWxmJyBodHRwczogOyBzY3JpcHQtc3JjICogJ3Vuc2FmZS1pbmxpbmUnICd1bnNhZmUtZXZhbCc7IHNjcmlwdC1zcmMtZWxlbSAqICogJ3Vuc2FmZS1pbmxpbmUnICd1bnNhZmUtZXZhbCcgOyBzY3JpcHQtc3JjLWF0dHIgKiAqICd1bnNhZmUtaW5saW5lJyAndW5zYWZlLWV2YWwnIDsgc3R5bGUtc3JjICogZGF0YTogJ3Vuc2FmZS1pbmxpbmUnOyBpbWctc3JjICogZGF0YTogOyBmb250LXNyYyAqIGRhdGE6IDsgY29ubmVjdC1zcmMgKjsgbWVkaWEtc3JjICo7IG9iamVjdC1zcmMgKjsgcHJlZmV0Y2gtc3JjICo7IGNoaWxkLXNyYyAqOyBmcmFtZS1zcmMgKjsgd29ya2VyLXNyYyAqOyBmcmFtZS1hbmNlc3RvcnMgKjsgZm9ybS1hY3Rpb24gJ3NlbGYnOyB1cGdyYWRlLWluc2VjdXJlLXJlcXVlc3RzOyBiYXNlLXVyaSAqOyBtYW5pZmVzdC1zcmMgKiIKCkhlYWRlciBhbHdheXMgc2V0aWZlbXB0eSBYLVhTUy1Qcm90ZWN0aW9uICIxIgoKCSMgVGhlIFNlcnZlck5hbWUgZGlyZWN0aXZlIHNldHMgdGhlIHJlcXVlc3Qgc2NoZW1lLCBob3N0bmFtZSBhbmQgcG9ydCB0aGF0CgkjIHRoZSBzZXJ2ZXIgdXNlcyB0byBpZGVudGlmeSBpdHNlbGYuIFRoaXMgaXMgdXNlZCB3aGVuIGNyZWF0aW5nCgkjIHJlZGlyZWN0aW9uIFVSTHMuIEluIHRoZSBjb250ZXh0IG9mIHZpcnR1YWwgaG9zdHMsIHRoZSBTZXJ2ZXJOYW1lCgkjIHNwZWNpZmllcyB3aGF0IGhvc3RuYW1lIG11c3QgYXBwZWFyIGluIHRoZSByZXF1ZXN0J3MgSG9zdDogaGVhZGVyIHRvCgkjIG1hdGNoIHRoaXMgdmlydHVhbCBob3N0LiBGb3IgdGhlIGRlZmF1bHQgdmlydHVhbCBob3N0ICh0aGlzIGZpbGUpIHRoaXMKCSMgdmFsdWUgaXMgbm90IGRlY2lzaXZlIGFzIGl0IGlzIHVzZWQgYXMgYSBsYXN0IHJlc29ydCBob3N0IHJlZ2FyZGxlc3MuCgkjIEhvd2V2ZXIsIHlvdSBtdXN0IHNldCBpdCBmb3IgYW55IGZ1cnRoZXIgdmlydHVhbCBob3N0IGV4cGxpY2l0bHkuCgkjU2VydmVyTmFtZSB3d3cuZXhhbXBsZS5jb20KCglTZXJ2ZXJBZG1pbiB3ZWJtYXN0ZXJAbG9jYWxob3N0CglEb2N1bWVudFJvb3QgL3Zhci93d3cvaHRtbAoKCSMgQXZhaWxhYmxlIGxvZ2xldmVsczogdHJhY2U4LCAuLi4sIHRyYWNlMSwgZGVidWcsIGluZm8sIG5vdGljZSwgd2FybiwKCSMgZXJyb3IsIGNyaXQsIGFsZXJ0LCBlbWVyZy4KCSMgSXQgaXMgYWxzbyBwb3NzaWJsZSB0byBjb25maWd1cmUgdGhlIGxvZ2xldmVsIGZvciBwYXJ0aWN1bGFyCgkjIG1vZHVsZXMsIGUuZy4KCSNMb2dMZXZlbCBpbmZvIHNzbDp3YXJuCgkKCUVycm9yTG9nIC9kZXYvc3RkZXJyCiMJQ3VzdG9tTG9nIC9kZXYvc3Rkb3V0IGNvbWJpbmVkCglDdXN0b21Mb2cgL2Rldi9udWxsIGNvbWJpbmVkCgoKPERpcmVjdG9yeSAiL3Zhci93d3cvaHRtbC9kYXRhIj4KICAgIFJlcXVpcmUgYWxsIGRlbmllZAo8L0RpcmVjdG9yeT4KPElmTW9kdWxlIG1vZF9mYXN0Y2dpLmM+ClNldEVudiBQSFBfQURNSU5fVkFMVUUgInBvc3RfbWF4X3NpemUgPSAxMjhNXG51cGxvYWRfbWF4X2ZpbGVzaXplID0gMTI4TSIgCkFkZFR5cGUgYXBwbGljYXRpb24veC1odHRwZC1mYXN0cGhwNyAucGhwCiBBY3Rpb24gYXBwbGljYXRpb24veC1odHRwZC1mYXN0cGhwNyAvcGhwNS1mY2dpCiBBbGlhcyAvcGhwNy1mY2dpIC91c3IvbGliL2NnaS1iaW4vcGhwNy1mY2dpCiBGYXN0Q2dpRXh0ZXJuYWxTZXJ2ZXIgL3Vzci9saWIvY2dpLWJpbi9waHA3LWZjZ2kgLXNvY2tldCAvdmFyL3J1bi9waHAtZnBtLnNvY2sgLXBhc3MtaGVhZGVyIEF1dGhvcml6YXRpb24KIDxEaXJlY3RvcnkgL3Vzci9saWIvY2dpLWJpbj4KICBSZXF1aXJlIGFsbCBncmFudGVkCiA8L0RpcmVjdG9yeT4KPC9JZk1vZHVsZT4KCgoJIyBGb3IgbW9zdCBjb25maWd1cmF0aW9uIGZpbGVzIGZyb20gY29uZi1hdmFpbGFibGUvLCB3aGljaCBhcmUKCSMgZW5hYmxlZCBvciBkaXNhYmxlZCBhdCBhIGdsb2JhbCBsZXZlbCwgaXQgaXMgcG9zc2libGUgdG8KCSMgaW5jbHVkZSBhIGxpbmUgZm9yIG9ubHkgb25lIHBhcnRpY3VsYXIgdmlydHVhbCBob3N0LiBGb3IgZXhhbXBsZSB0aGUKCSMgZm9sbG93aW5nIGxpbmUgZW5hYmxlcyB0aGUgQ0dJIGNvbmZpZ3VyYXRpb24gZm9yIHRoaXMgaG9zdCBvbmx5CgkjIGFmdGVyIGl0IGhhcyBiZWVuIGdsb2JhbGx5IGRpc2FibGVkIHdpdGggImEyZGlzY29uZiIuCgkjSW5jbHVkZSBjb25mLWF2YWlsYWJsZS9zZXJ2ZS1jZ2ktYmluLmNvbmYKPC9WaXJ0dWFsSG9zdD4KCiMgdmltOiBzeW50YXg9YXBhY2hlIHRzPTQgc3c9NCBzdHM9NCBzciBub2V0Cg==" |base64 -d > /etc/apache2/sites-available/000-default.conf
##ADJUST UPLOAD SIZE
RUN  find /etc/php* -name php.ini |xargs sed 's/.\+\(upload_max_filesize\|post_max_size\).\+//g' -i
RUN /bin/bash -c "(echo ;echo 'php_admin_flag[log_errors] = on';echo 'php_admin_value[error_log] = /dev/stderr';echo 'php_value[upload_max_filesize] = 128M';echo 'php_admin_value[upload_max_filesize] = 256M';echo 'php_admin_value[post_max_size] = 256M') |tee /etc/php/8.1/fpm/pool.d/www.conf -a"
RUN /bin/bash -c 'find /etc/php* -name php.ini |while read phpini;do ( echo;echo upload_max_filesize=256M;echo post_max_size=128M ) |tee -a $phpini;done '
RUN a2enmod actions

RUN ( test -e /var/www/html/index.html && rm /var/www/html/index.html )||true
VOLUME /var/www/html/data

EXPOSE 8888 443 22
VOLUME /etc/apache2/sites-enabled/
WORKDIR /var/www
COPY entry-painloop.sh /

CMD /bin/bash /entry-painloop.sh
